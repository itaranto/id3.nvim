#!/bin/sh
set -eu

if ! command -v jq > /dev/null 2>&1; then
    echo 'Please install "jq"'
    exit 1
fi

src_dir=$(dirname "${0}")/..
report_path=check.json

# shellcheck disable=SC2064
trap "rm -f ${report_path}" INT TERM EXIT

# The `--configpath` option is a workaround for making it pick up the right Lua version.
# See :https://github.com/LuaLS/lua-language-server/issues/2852
lua-language-server \
    --configpath "${src_dir}/.luarc.json" \
    --logpath . \
    --loglevel error \
    --checklevel Hint \
    --check "${src_dir}"

if [ "$(jq 'to_entries|length' "${report_path}")" -gt 0 ]; then
    cat "${report_path}" 1>&2
    exit 1
fi
