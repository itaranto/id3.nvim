#!/bin/sh
set -eu

install_dir=${HOME}/.local/share/lua-language-server
bin_dir=${HOME}/.local/bin

mkdir -p "${install_dir}" "${bin_dir}"

curl \
    --silent \
    --location \
    https://github.com/LuaLS/lua-language-server/releases/download/3.10.6/lua-language-server-3.10.6-linux-x64.tar.gz | \
    tar -xvz -C "${install_dir}"

ln -s "${install_dir}/bin/lua-language-server" "${bin_dir}/lua-language-server"
