# id3.nvim

This plugin allows editing MP3 and FLAC tags with Neovim.

It's heavily inspired by [id3.vim](https://github.com/AndrewRadev/id3.vim) but written in Lua.

![](./assets/demo.gif)

## Installation

Install with [lazy.nvim](https://github.com/folke/lazy.nvim):

```lua
{
  'https://gitlab.com/itaranto/id3.nvim',
  version = '*',
  config = function() require('id3').setup() end,
}
```

Install with [packer.nvim](https://github.com/wbthomason/packer.nvim):

```lua
use {
  'https://gitlab.com/itaranto/id3.nvim',
  tag = '*',
  config = function() require('id3').setup() end
}
```

## Dependencies

### MP3 files

To edit an MP3 file, you'll need a command-line ID3 tag editor installed.

This plugin works with the following:

1. `id3`
2. `id3v2`
3. `id3lib`
4. `mid3v2`

You should be able to install any of these with your system's package manager. For example, on Arch
Linux:

```sh
paru -S id3
```

> **NOTE:** If you're using other tools which write ID3v2.4 tags such as
> [Beets](https://beets.readthedocs.io/en/stable/) or
> [MusicBrainz Picard](https://picard.musicbrainz.org/) you will need to use the
> [mid3v2](https://mutagen.readthedocs.io/en/latest/man/mid3v2.html) tool as it is the only option
> provided that supports the v2.4 specification.

### FLAC files

To edit a FLAC file, you'll need a command-line FLAC tag editor installed.

This plugin works with the `metaflac` command, which, on Arch Linux, comes in the `flac` package:

```sh
sudo pacman -S flac
```

## Configuration

To use the default configuration, do:

```lua
require('id3').setup()
```

The default values are:

```lua
local default_config = {
  mp3_tool = 'id3',
  flac_tool = 'metaflac',
}
```

Alternatively, you can change some of the settings:

```lua
require('id3').setup({ mp3_tool = 'id3v2' })
```

## Usage

Edit an MP3 or FLAC file. You'll see a buffer with its metadata stored as tags.

Writing the buffer will update the tags and rename the file accordingly.

## Known issues

### id3lib

`id3info` can actually read all supported frames, but its writing counterpart, `id3tag`, can only
write the most common ones. It will also zero out any additional tags when doing so.

Setting the genre tag with `id3tag` is not supported by `id3.nvim`: `id3info` can read the genre as
either number or string. However, `id3tag` can only set the genre using the numeric identifier.

## TODO

- Syntax highlighting.

## Contributing

*"If your commit message sucks, I'm not going to accept your pull request."*

Commit messages should follow the standard Git "imperative" convention as defined
[here](https://cbea.ms/git-commit/#seven-rules).

Additionally, please note that this repository is configured to squash merge request commits. The
resulting commit message will be generated from both the MR title and description.
