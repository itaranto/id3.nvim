local assert = require('luassert.assert')
local mock = require('luassert.mock')

local buffer = require('id3.buffer')

-- This is required since busted/luassert requires an existing table to be patched.
local parser_spec = {
  read = function(_) end,
  write = function(_, _) end,
}

local test_tags = {
  { name = 'Album',        value = 'album'        },
  { name = 'Album artist', value = 'album artist' },
  { name = 'Artist',       value = 'artist'       },
  { name = 'Genre',        value = 'genre'        },
  { name = 'Title',        value = 'title'        },
  { name = 'Track',        value = '01'           },
  { name = 'Track total',  value = '10'           },
  { name = 'Year',         value = '2006'         },
}

describe('buffer.read_tags', function()
  local parser_mock
  local vim_api_mock

  before_each(function()
    parser_mock = mock(parser_spec, true)
    vim_api_mock = mock(vim.api, true)
  end)

  after_each(function()
    mock.revert(parser_mock)
    mock.revert(vim_api_mock)
  end)

  it('should set lines correclty with empty tags', function()
    parser_mock.read.returns({})

    buffer.read_tags(parser_mock, 'filename')

    assert.stub(vim_api_mock.nvim_buf_set_lines).was_called_with(0, 0, -1, true, {
      'filename',
      '========',
      '',
    })
  end)

  it('should set lines correclty with valid tags', function()
    parser_mock.read.returns(test_tags)

    buffer.read_tags(parser_mock, 'filename')

    assert.stub(vim_api_mock.nvim_buf_set_lines).was_called_with(0, 0, -1, true, {
      'filename',
      '========',
      '',
      'Album: album',
      'Album artist: album artist',
      'Artist: artist',
      'Genre: genre',
      'Title: title',
      'Track: 01',
      'Track total: 10',
      'Year: 2006',
    })
  end)
end)

describe('buffer.write_tags', function()
  local vim_api_mock
  local parser_mock
  local os_mock
  local vim_opt

  before_each(function()
    vim_api_mock = mock(vim.api, true)
    parser_mock = mock(parser_spec, true)
    os_mock = mock(os, true)
    -- It seems luassert/busted doesn't support patching tables.
    vim_opt = vim.opt
    vim.opt = { modified = false }
  end)

  after_each(function()
    vim.opt = vim_opt
    mock.revert(os_mock)
    mock.revert(parser_mock)
    mock.revert(vim_api_mock)
  end)

  it('should write tags correctly for empty values', function()
    vim_api_mock.nvim_buf_get_lines.returns({
      'filename',
      '========',
      '',
      'Album: ',
      'Album artist: ',
      'Artist: ',
      'Genre: ',
      'Title: ',
      'Track: ',
      'Track total: ',
      'Year: ',
    })

    buffer.write_tags(parser_mock, 'filename')

    assert.stub(vim_api_mock.nvim_buf_get_lines).was_called_with(0, 0, -1, true)
    assert.stub(parser_mock.write).was_called_with('filename', {})
  end)

  it('should write tags correctly', function()
    vim_api_mock.nvim_buf_get_lines.returns({
      'filename',
      '========',
      '',
      'Album: album',
      'Album artist: album artist',
      'Artist: artist',
      'Genre: genre',
      'Title: title',
      'Track: 01',
      'Track total: 10',
      'Year: 2006',
    })

    buffer.write_tags(parser_mock, 'filename')

    assert.stub(vim_api_mock.nvim_buf_get_lines).was_called_with(0, 0, -1, true)
    assert.stub(parser_mock.write).was_called_with('filename', test_tags)
  end)

  it('should write tags correctly and rename the file', function()
    vim_api_mock.nvim_buf_get_lines.returns({
      'new-filename',
      '============',
      '',
      'Album: album',
      'Album artist: album artist',
      'Artist: artist',
      'Genre: genre',
      'Title: title',
      'Track: 01',
      'Track total: 10',
      'Year: 2006',
    })

    os_mock.rename.returns(true, nil)
    parser_mock.read.returns(test_tags)

    buffer.write_tags(parser_mock, 'filename')

    assert.stub(vim_api_mock.nvim_buf_get_lines).was_called_with(0, 0, -1, true)
    assert.stub(parser_mock.write).was_called_with('filename', test_tags)
    assert.stub(os_mock.rename).was_called_with('filename', 'new-filename')
    assert.stub(vim_api_mock.nvim_buf_set_name).was_called_with(0, 'new-filename')
  end)
end)
