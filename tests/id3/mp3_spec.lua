local assert = require('luassert.assert')
local mock = require('luassert.mock')

local job = require('id3.job')
local mp3 = require('id3.mp3')
local values = require('tests.id3.util').values_iter

---@param tags id3.Tags
---@return nil
local function sort_tags(tags)
  table.sort(tags, function(a, b)
    return a.name < b.name
  end)
end

local all_empty_tag_values = {
  { name = 'Album',        value = '' },
  { name = 'Album artist', value = '' },
  { name = 'Artist',       value = '' },
  { name = 'Genre',        value = '' },
  { name = 'Title',        value = '' },
  { name = 'Track',        value = '' },
  { name = 'Year',         value = '' },
}

local empty_tag_values = {
  { name = 'Album',        value = 'album'        },
  { name = 'Album artist', value = 'album artist' },
  { name = 'Artist',       value = 'artist'       },
  { name = 'Genre',        value = 'genre'        },
  { name = 'Title',        value = ''             },
  { name = 'Track',        value = '01/10'        },
  { name = 'Year',         value = ''             },
}

local missing_tag_values = {
  { name = 'Album',        value = 'album'        },
  { name = 'Album artist', value = 'album artist' },
  { name = 'Artist',       value = 'artist'       },
  { name = 'Genre',        value = 'genre'        },
  { name = 'Track',        value = '01/10'        },
}

local complete_tags = {
  { name = 'Album',        value = 'album'        },
  { name = 'Album artist', value = 'album artist' },
  { name = 'Artist',       value = 'artist'       },
  { name = 'Genre',        value = 'genre'        },
  { name = 'Title',        value = 'title'        },
  { name = 'Track',        value = '01/10'        },
  { name = 'Year',         value = '2006'         },
}

describe('mp3.ID3Parser', function()
  local parser = mp3.ID3Parser

  ---@format disable-next
  local read_tags_cmd = {
    'id3', '-q',
    table.concat({
      'TIT2=%_{TIT2}\\n',
      'TPE1=%_{TPE1}\\n',
      'TPE2=%_{TPE2}\\n',
      'TALB=%_{TALB}\\n',
      'TYER=%_{TYER}\\n',
      'TRCK=%_{TRCK}\\n',
      'TCON=%_{TCON}\\n',
    }),
    'filename.mp3',
  }

  local job_mock

  before_each(function()
    job_mock = mock(job, true)
  end)

  after_each(function()
    mock.revert(job_mock)
  end)

  describe('read', function()
    it('should return no values for empty output', function()
      job_mock.run.returns(values({}))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(all_empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return no values malformed output', function()
      job_mock.run.returns(values({ 'malformed' }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(all_empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return correct values for missing tags output', function()
      job_mock.run.returns(values({
        'TPE1=artist',
        'TPE2=album artist',
        'TALB=album',
        'TRCK=01/10',
        'TCON=genre',
      }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return correct values for complete tags output', function()
      job_mock.run.returns(values({
        'TIT2=title',
        'TPE1=artist',
        'TPE2=album artist',
        'TALB=album',
        'TYER=2006',
        'TRCK=01/10',
        'TCON=genre',
      }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(complete_tags, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)
  end)

  describe('write', function()
    it('should build the correct command for empty tags', function()
      parser.write('filename.mp3', {})
      assert.stub(job_mock.run).was_called_with({ 'id3', '-2', 'filename.mp3' })
    end)

    it('should build the correct command for missing tag values', function()
      parser.write('filename.mp3', missing_tag_values)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'id3', '-2',
        '-wTALB', 'album',
        '-wTPE2', 'album artist',
        '-wTPE1', 'artist',
        '-wTCON', 'genre',
        '-wTRCK', '01/10',
        'filename.mp3',
      })
    end)

    it('should build the correct command for empty tag values', function()
      parser.write('filename.mp3', empty_tag_values)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'id3', '-2',
        '-wTALB', 'album',
        '-wTPE2', 'album artist',
        '-wTPE1', 'artist',
        '-wTCON', 'genre',
        '-wTIT2', '',
        '-wTRCK', '01/10',
        '-wTYER', '',
        'filename.mp3',
      })
    end)

    it('should build the correct command for complete tags', function()
      parser.write('filename.mp3', complete_tags)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'id3', '-2',
        '-wTALB', 'album',
        '-wTPE2', 'album artist',
        '-wTPE1', 'artist',
        '-wTCON', 'genre',
        '-wTIT2', 'title',
        '-wTRCK', '01/10',
        '-wTYER', '2006',
        'filename.mp3',
      })
    end)
  end)
end)

describe('mp3.ID3v2Parser', function()
  local parser = mp3.ID3V2Parser
  local read_tags_cmd = { 'id3v2', '-l', 'filename.mp3' }

  local job_mock

  before_each(function()
    job_mock = mock(job, true)
  end)

  after_each(function()
    mock.revert(job_mock)
  end)

  describe('read', function()
    it('should return no values for empty output', function()
      job_mock.run.returns(values({}))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(all_empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return no values malformed output', function()
      job_mock.run.returns(values({ 'malformed' }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(all_empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return correct values for missing tags output', function()
      job_mock.run.returns(values({
        'id3v2 tag info for filename.mp3:',
        'TPE1 (Lead performer(s)/Soloist(s)): artist',
        'TPE2 (Band/orchestra/accompaniment): album artist',
        'TALB (Album/Movie/Show title): album',
        'TRCK (Track number/Position in set): 01/10',
        'TCON (Content type): genre (666)',
        'filename.mp3: No ID3v1 tag',
      }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return correct values for complete tags output', function()
      job_mock.run.returns(values({
        'id3v2 tag info for filename.mp3:',
        'TIT2 (Title/songname/content description): title',
        'TPE1 (Lead performer(s)/Soloist(s)): artist',
        'TPE2 (Band/orchestra/accompaniment): album artist',
        'TALB (Album/Movie/Show title): album',
        'TYER (Year): 2006',
        'TRCK (Track number/Position in set): 01/10',
        'TCON (Content type): genre (666)',
        'filename.mp3: No ID3v1 tag',
      }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(complete_tags, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
      mock.revert(job_mock)
    end)
  end)

  describe('write', function()
    it('should build the correct command for empty tags', function()
      parser.write('filename.mp3', {})
      assert.stub(job_mock.run).was_called_with({ 'id3v2', '-2', 'filename.mp3' })
    end)

    it('should build the correct command for missing tag values', function()
      parser.write('filename.mp3', missing_tag_values)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'id3v2', '-2',
        '--TALB', 'album',
        '--TPE2', 'album artist',
        '--TPE1', 'artist',
        '--TCON', 'genre',
        '--TRCK', '01/10',
        'filename.mp3',
      })
    end)

    it('should build the correct command for empty tag values', function()
      parser.write('filename.mp3', empty_tag_values)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'id3v2', '-2',
        '--TALB', 'album',
        '--TPE2', 'album artist',
        '--TPE1', 'artist',
        '--TCON', 'genre',
        '--TIT2', '',
        '--TRCK', '01/10',
        '--TYER', '',
        'filename.mp3',
      })
    end)

    it('should build the correct command for complete tags', function()
      parser.write('filename.mp3', complete_tags)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'id3v2', '-2',
        '--TALB', 'album',
        '--TPE2', 'album artist',
        '--TPE1', 'artist',
        '--TCON', 'genre',
        '--TIT2', 'title',
        '--TRCK', '01/10',
        '--TYER', '2006',
        'filename.mp3',
      })
    end)
  end)
end)

describe('mp3.ID3LibParser', function()
  local parser = mp3.ID3LibParser
  local read_tags_cmd = { 'id3info', 'filename.mp3' }

  local job_mock

  before_each(function()
    job_mock = mock(job, true)
  end)

  after_each(function()
    mock.revert(job_mock)
  end)

  describe('read', function()
    it('should return no values for empty output', function()
      job_mock.run.returns(values({}))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(all_empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return no values malformed output', function()
      job_mock.run.returns(values({ 'malformed' }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(all_empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return correct values for missing tags output', function()
      job_mock.run.returns(values({
        '',
        '*** Tag information for filename.mp3',
        '=== TPE1 (Lead performer(s)/Soloist(s)): artist',
        '=== TPE2 (Band/orchestra/accompaniment): album artist',
        '=== TALB (Album/Movie/Show title): album',
        '=== TRCK (Track number/Position in set): 01/10',
        '=== TCON (Content type): genre',
        '*** mp3 info',
        'MPEG1/layer III',
        'Bitrate: 128KBps',
        'Frequency: 44KHz',
      }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return correct values for complete tags output', function()
      job_mock.run.returns(values({
        '',
        '*** Tag information for filename.mp3',
        '=== TIT2 (Title/songname/content description): title',
        '=== TPE1 (Lead performer(s)/Soloist(s)): artist',
        '=== TPE2 (Band/orchestra/accompaniment): album artist',
        '=== TALB (Album/Movie/Show title): album',
        '=== TYER (Year): 2006',
        '=== TRCK (Track number/Position in set): 01/10',
        '=== TCON (Content type): genre',
        '*** mp3 info',
        'MPEG1/layer III',
        'Bitrate: 128KBps',
        'Frequency: 44KHz',
      }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(complete_tags, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)
  end)

  describe('write', function()
    it('should build the correct command for empty tags', function()
      parser.write('filename.mp3', {})
      assert.stub(job_mock.run).was_called_with({ 'id3tag', '-2', 'filename.mp3' })
    end)

    it('should build the correct command for missing tag values', function()
      parser.write('filename.mp3', missing_tag_values)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'id3tag', '-2',
        '--album', 'album',
        '--artist', 'artist',
        '--track', '01',
        '--total', '10',
        'filename.mp3',
      })
    end)

    it('should build the correct command for empty tag values', function()
      parser.write('filename.mp3', empty_tag_values)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'id3tag', '-2',
        '--album', 'album',
        '--artist', 'artist',
        '--song', '',
        '--track', '01',
        '--total', '10',
        '--year', '',
        'filename.mp3',
      })
    end)

    it('should build the correct command for complete tags', function()
      parser.write('filename.mp3', complete_tags)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'id3tag', '-2',
        '--album', 'album',
        '--artist', 'artist',
        '--song', 'title',
        '--track', '01',
        '--total', '10',
        '--year', '2006',
        'filename.mp3',
      })
    end)
  end)
end)

describe('mp3.Mid3v2Parser', function()
  local parser = mp3.Mid3v2Parser
  local read_tags_cmd = { 'mid3v2', 'filename.mp3' }

  local job_mock

  before_each(function()
    job_mock = mock(job, true)
  end)

  after_each(function()
    mock.revert(job_mock)
  end)

  describe('read', function()
    it('should return no values for empty output', function()
      job_mock.run.returns(values({}))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(all_empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return no values malformed output', function()
      job_mock.run.returns(values({ 'malformed' }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(all_empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return correct values for missing tags output', function()
      job_mock.run.returns(values({
        'TPE1=artist',
        'TPE2=album artist',
        'TALB=album',
        'TRCK=01/10',
        'TCON=genre',
      }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return correct values for complete tags output', function()
      job_mock.run.returns(values({
        'TIT2=title',
        'TPE1=artist',
        'TPE2=album artist',
        'TALB=album',
        'TDRC=2006',
        'TRCK=01/10',
        'TCON=genre',
      }))

      local tags = parser.read('filename.mp3')
      sort_tags(tags)

      assert.are.same(complete_tags, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)
  end)

  describe('write', function()
    it('should build the correct command for empty tags', function()
      parser.write('filename.mp3', {})
      assert.stub(job_mock.run).was_called_with({ 'mid3v2', 'filename.mp3' })
    end)

    it('should build the correct command for missing tag values', function()
      parser.write('filename.mp3', missing_tag_values)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'mid3v2',
        '--TALB', 'album',
        '--TPE2', 'album artist',
        '--TPE1', 'artist',
        '--TCON', 'genre',
        '--TRCK', '01/10',
        'filename.mp3',
      })
    end)

    it('should build the correct command for empty tag values', function()
      parser.write('filename.mp3', empty_tag_values)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'mid3v2',
        '--TALB', 'album',
        '--TPE2', 'album artist',
        '--TPE1', 'artist',
        '--TCON', 'genre',
        '--TIT2', '',
        '--TRCK', '01/10',
        '--TYER', '',
        'filename.mp3',
      })
    end)

    it('should build the correct command for complete tags', function()
      parser.write('filename.mp3', complete_tags)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'mid3v2',
        '--TALB', 'album',
        '--TPE2', 'album artist',
        '--TPE1', 'artist',
        '--TCON', 'genre',
        '--TIT2', 'title',
        '--TRCK', '01/10',
        '--TYER', '2006',
        'filename.mp3',
      })
    end)
  end)
end)
