M = {}

---@generic T : integer
---@param list T[]
---@return fun(): T?
function M.values_iter(list)
  local i = 0
  return function()
    i = i + 1
    return list[i]
  end
end

return M
