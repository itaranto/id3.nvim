local assert = require('luassert.assert')
local mock = require('luassert.mock')

local flac = require('id3.flac')
local job = require('id3.job')
local values = require('tests.id3.util').values_iter

---@param tags id3.Tags
---@return nil
local function sort_tags(tags)
  table.sort(tags, function(a, b)
    return a.name < b.name
  end)
end

local all_empty_tag_values = {
  { name = 'Album',        value = '' },
  { name = 'Album artist', value = '' },
  { name = 'Artist',       value = '' },
  { name = 'Genre',        value = '' },
  { name = 'Title',        value = '' },
  { name = 'Track',        value = '' },
  { name = 'Track total',  value = '' },
  { name = 'Year',         value = '' },
}

local empty_tag_values = {
  { name = 'Album',        value = 'album'        },
  { name = 'Album artist', value = 'album artist' },
  { name = 'Artist',       value = 'artist'       },
  { name = 'Genre',        value = 'genre'        },
  { name = 'Title',        value = ''             },
  { name = 'Track',        value = '01'           },
  { name = 'Track total',  value = '10'           },
  { name = 'Year',         value = ''             },
}

local missing_tag_values = {
  { name = 'Album',        value = 'album'        },
  { name = 'Album artist', value = 'album artist' },
  { name = 'Artist',       value = 'artist'       },
  { name = 'Genre',        value = 'genre'        },
  { name = 'Track',        value = '01'           },
  { name = 'Track total',  value = '10'           },
}

local complete_tags = {
  { name = 'Album',        value = 'album'        },
  { name = 'Album artist', value = 'album artist' },
  { name = 'Artist',       value = 'artist'       },
  { name = 'Genre',        value = 'genre'        },
  { name = 'Title',        value = 'title'        },
  { name = 'Track',        value = '01'           },
  { name = 'Track total',  value = '10'           },
  { name = 'Year',         value = '2006'         },
}

describe('flac.MetaFLACParser', function()
  local parser = flac.MetaFLACParser

  local job_mock

  before_each(function()
    job_mock = mock(job, true)
  end)

  after_each(function()
    mock.revert(job_mock)
  end)

  describe('read', function()
    local read_tags_cmd = { 'metaflac', '--export-tags-to', '-', 'filename.flac' }

    it('should return no values for empty output', function()
      job_mock.run.returns(values({}))

      local tags = parser.read('filename.flac')
      sort_tags(tags)

      assert.are.same(all_empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return no values malformed output', function()
      job_mock.run.returns(values({ 'malformed' }))

      local tags = parser.read('filename.flac')
      sort_tags(tags)

      assert.are.same(all_empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return correct values for missing tags output', function()
      job_mock.run.returns(values({
        'ARTIST=artist',
        'ALBUMARTIST=album artist',
        'ALBUM=album',
        'TRACKNUMBER=01',
        'TRACKTOTAL=10',
        'GENRE=genre',
      }))

      local tags = parser.read('filename.flac')
      sort_tags(tags)

      assert.are.same(empty_tag_values, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)

    it('should return correct values for complete tags output', function()
      job_mock.run.returns(values({
        'TITLE=title',
        'ARTIST=artist',
        'ALBUMARTIST=album artist',
        'ALBUM=album',
        'DATE=2006',
        'TRACKNUMBER=01',
        'TRACKTOTAL=10',
        'GENRE=genre',
      }))

      local tags = parser.read('filename.flac')
      sort_tags(tags)

      assert.are.same(complete_tags, tags)
      assert.stub(job_mock.run).was_called_with(read_tags_cmd)
    end)
  end)

  describe('write', function()
    it('should build the correct command for empty tags', function()
      parser.write('filename.flac', {})
      assert.stub(job_mock.run).was_called_with({ 'metaflac', 'filename.flac' })
    end)

    it('should build the correct command for missing tag values', function()
      parser.write('filename.flac', missing_tag_values)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'metaflac',
        '--remove-tag', 'ALBUM', '--set-tag', 'ALBUM=album',
        '--remove-tag', 'ALBUMARTIST', '--set-tag', 'ALBUMARTIST=album artist',
        '--remove-tag', 'ARTIST', '--set-tag', 'ARTIST=artist',
        '--remove-tag', 'GENRE', '--set-tag', 'GENRE=genre',
        '--remove-tag', 'TRACKNUMBER', '--set-tag', 'TRACKNUMBER=01',
        '--remove-tag', 'TRACKTOTAL', '--set-tag', 'TRACKTOTAL=10',
        'filename.flac',
      })
    end)

    it('should build the correct command for empty tag values', function()
      parser.write('filename.flac', empty_tag_values)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'metaflac',
        '--remove-tag', 'ALBUM', '--set-tag', 'ALBUM=album',
        '--remove-tag', 'ALBUMARTIST', '--set-tag', 'ALBUMARTIST=album artist',
        '--remove-tag', 'ARTIST', '--set-tag', 'ARTIST=artist',
        '--remove-tag', 'GENRE', '--set-tag', 'GENRE=genre',
        '--remove-tag', 'TITLE', '--set-tag', 'TITLE=',
        '--remove-tag', 'TRACKNUMBER', '--set-tag', 'TRACKNUMBER=01',
        '--remove-tag', 'TRACKTOTAL', '--set-tag', 'TRACKTOTAL=10',
        '--remove-tag', 'DATE', '--set-tag', 'DATE=',
        'filename.flac',
      })
    end)

    it('should build the correct command for complete tags', function()
      parser.write('filename.flac', complete_tags)

      ---@format disable-next
      assert.stub(job_mock.run).was_called_with({
        'metaflac',
        '--remove-tag', 'ALBUM', '--set-tag', 'ALBUM=album',
        '--remove-tag', 'ALBUMARTIST', '--set-tag', 'ALBUMARTIST=album artist',
        '--remove-tag', 'ARTIST', '--set-tag', 'ARTIST=artist',
        '--remove-tag', 'GENRE', '--set-tag', 'GENRE=genre',
        '--remove-tag', 'TITLE', '--set-tag', 'TITLE=title',
        '--remove-tag', 'TRACKNUMBER', '--set-tag', 'TRACKNUMBER=01',
        '--remove-tag', 'TRACKTOTAL', '--set-tag', 'TRACKTOTAL=10',
        '--remove-tag', 'DATE', '--set-tag', 'DATE=2006',
        'filename.flac',
      })
    end)
  end)
end)
