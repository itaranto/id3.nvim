local buffer = require('id3.buffer')
local flac = require('id3.flac')
local mp3 = require('id3.mp3')

local M = {}

---@class id3.Options
---@field mp3_tool? string
---@field flac_tool? string

---@alias id3.Parser { read: id3.ReadFun, write: id3.WriteFun }

---@alias id3.Reader { read: id3.ReadFun }

---@alias id3.ReadFun fun(filename: string): id3.Tags

---@alias id3.WriteFun fun(filename: string, tags: id3.Tags): nil

---@alias id3.Tags { name: string, value: string }[]

---@param dst table
---@param src? table
---@return table
local function merge_config(dst, src)
  return vim.tbl_extend('force', dst, src or {})
end

---@param tool string
---@return id3.Parser?
local function create_mp3_parser(tool)
  local parser
  if tool == 'id3' then
    parser = mp3.ID3Parser
  elseif tool == 'id3v2' then
    parser = mp3.ID3V2Parser
  elseif tool == 'id3lib' then
    parser = mp3.ID3LibParser
  elseif tool == 'mid3v2' then
    parser = mp3.Mid3v2Parser
  else
    print(string.format('[id3.nvim] Invalid MP3 command-line tool "%s"', tool))
  end

  return parser
end

---@param tool string
---@return id3.Parser?
local function create_flac_parser(tool)
  local parser
  if tool == 'metaflac' then
    parser = flac.MetaFLACParser
  else
    print(string.format('[id3.nvim] Invalid FLAC command-line tool "%s"', tool))
  end

  return parser
end

---@param group number
---@param pattern string
---@param parser id3.Reader
---@return nil
local function create_read_autocmd(group, pattern, parser)
  vim.api.nvim_create_autocmd('BufReadCmd', {
    group = group,
    pattern = pattern,
    callback = function(args)
      local status, err = pcall(buffer.read_tags, parser, args.file)
      if not status then
        print(string.format('[id3.nvim] Failed to read buffer "%s"\n%s', args.file, err))
      end
    end,
  })
end

---@param group number
---@param pattern string
---@param parser id3.Parser
---@return nil
local function create_write_autocmd(group, pattern, parser)
  vim.api.nvim_create_autocmd('BufWriteCmd', {
    group = group,
    pattern = pattern,
    callback = function(args)
      local status, result = pcall(buffer.write_tags, parser, args.file)
      if not status then
        print(string.format('[id3.nvim] Failed to write buffer "%s"\n%s', args.file, result))
      end
    end,
  })
end

---@param group number
---@param pattern string
---@param parser id3.Parser
local function create_autocmds(group, pattern, parser)
  create_read_autocmd(group, pattern, parser)
  create_write_autocmd(group, pattern, parser)
end

---@param config? id3.Options
---@return nil
function M.setup(config)
  local default_config = {
    mp3_tool = 'id3',
    flac_tool = 'metaflac',
  }

  config = merge_config(default_config, config)

  local group = vim.api.nvim_create_augroup('ID3AutoGroup', {})

  local mp3_parser = create_mp3_parser(config.mp3_tool)
  if mp3_parser then
    create_autocmds(group, '*.mp3', mp3_parser)
  end

  local flac_parser = create_flac_parser(config.flac_tool)
  if flac_parser then
    create_autocmds(group, '*.flac', flac_parser)
  end
end

return M
