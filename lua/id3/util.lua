local M = {}

---@param list table
---@param ... any
---@return nil
function M.table_insert(list, ...)
  for _, value in ipairs({ ... }) do
    table.insert(list, value)
  end
end

return M
