local common = require('id3.common')
local job = require('id3.job')
local util = require('id3.util')

local M = {}

---@type { [string]: string }
local frame_to_name = {
  TIT2 = 'Title',
  TPE1 = 'Artist',
  TPE2 = 'Album artist',
  TALB = 'Album',
  TYER = 'Year',
  TRCK = 'Track',
  TCON = 'Genre',
}

---@type { [string]: string }
local name_to_frame = {
  Title            = 'TIT2',
  Artist           = 'TPE1',
  ['Album artist'] = 'TPE2',
  Album            = 'TALB',
  Year             = 'TYER',
  Track            = 'TRCK',
  Genre            = 'TCON',
}

---@class mp3.ID3Parser
M.ID3Parser = {}

---@param filename string
---@return id3.Tags
function M.ID3Parser.read(filename)
  local frames = {
    'TIT2',
    'TPE1',
    'TPE2',
    'TALB',
    'TYER',
    'TRCK',
    'TCON',
  }

  -- Build a formatting pattern of the form:
  -- FRAME_1=%_{FRAME_1}\n...FRAME_N=%_{FRAME_N}\n
  local pattern_builder = {}
  for _, frame in ipairs(frames) do
    table.insert(pattern_builder, string.format('%s=%%_{%s}\\n', frame, frame))
  end

  local pattern = table.concat(pattern_builder)
  local command = { 'id3', '-q', pattern, filename }
  local lines = job.run(command)

  local values = {}
  for line in lines do
    local frame, value = line:match('^(.+)=(.+)$')
    if frame and value then
      values[frame] = value
    end
  end

  return common.make_tags(frame_to_name, values)
end

---@param filename string
---@param tags id3.Tags
---@return nil
function M.ID3Parser.write(filename, tags)
  local command = { 'id3', '-2' }

  -- Build command-line arguments of the form:
  -- -wFRAME_1 VALUE_1 ... -wFRAME_N VALUE_N
  for _, tag in ipairs(tags) do
    local frame = name_to_frame[tag.name]
    if frame then
      util.table_insert(command, string.format('-w%s', frame), tag.value)
    end
  end

  table.insert(command, filename)
  job.run(command)
end

---@class mp3.ID3V2Parser
M.ID3V2Parser = {}

---@param filename string
---@return id3.Tags
function M.ID3V2Parser.read(filename)
  local command = { 'id3v2', '-l', filename }
  local lines = job.run(command)

  local values = {}
  for line in lines do
    local frame, value = line:match('^(%w+) %(.+%): (.+)$')
    if frame and value then
      -- id3v2 returns the genre as "Genre Value (n)" where "n" is the genre's numeric identifier.
      -- In order to correctly write back the genre, we need to get rid of this number.
      if frame == 'TCON' then
        value = value:match('^(.*) %(%d+%)$')
      end

      values[frame] = value
    end
  end

  return common.make_tags(frame_to_name, values)
end

---@param filename string
---@param tags id3.Tags
---@return nil
function M.ID3V2Parser.write(filename, tags)
  local command = { 'id3v2', '-2' }

  -- Build command-line arguments of the form:
  -- --FRAME_1 VALUE_1 ... --FRAME_N VALUE_N
  for _, tag in ipairs(tags) do
    local frame = name_to_frame[tag.name]
    if frame then
      util.table_insert(command, string.format('--%s', frame), tag.value)
    end
  end

  table.insert(command, filename)
  job.run(command)
end

---@class mp3.ID3LibParser
M.ID3LibParser = {}

---@param filename string
---@return id3.Tags
function M.ID3LibParser.read(filename)
  local command = { 'id3info', filename }
  local lines = job.run(command)

  local values = {}
  for line in lines do
    local frame, value = line:match('^=== (%w+) %(.+%): (.+)$')
    if frame and value then
      values[frame] = value
    end
  end

  return common.make_tags(frame_to_name, values)
end

---@param filename string
---@param tags id3.Tags
---@return nil
function M.ID3LibParser.write(filename, tags)
  local command = { 'id3tag', '-2' }

  -- id3info can actually read all supported frames, but its writing counterpart, id3tag, can only
  -- write the most common ones. All other tags will be zeroed out.
  --
  -- The genre (TCON) tag won't be updated because, although id3info can read the genre as either a
  -- number or string, id3tag can only set the genre using the numeric identifier.
  local frame_to_arg = {
    TIT2 = 'song',
    TPE1 = 'artist',
    TALB = 'album',
    TYER = 'year',
    TRCK = { track = 'track', total = 'total' },
  }

  -- Build command-line arguments of the form:
  -- --song SONG ... --genre GENRE
  for _, tag in ipairs(tags) do
    local frame = name_to_frame[tag.name]
    if frame then
      local arg = frame_to_arg[frame]
      if arg then
        local arg_format = '--%s'

        -- id3tag handles TRCK in a special way, it must be done through 2 different arguments: the
        -- current track number and the total number of tracks.
        if frame == 'TRCK' then
          local track, total = tag.value:match('(%d+)/?(%d*)')
          if track then
            util.table_insert(command, string.format(arg_format, arg.track), track)
          end
          -- TODO: Check if the regex can be modified so it returns "nil" when the total is not
          -- present.
          if #total > 0 then
            util.table_insert(command, string.format(arg_format, arg.total), total)
          end
        else
          util.table_insert(command, string.format(arg_format, arg), tag.value)
        end
      end
    end
  end

  table.insert(command, filename)
  job.run(command)
end

---@class mp3.Mid3v2Parser
M.Mid3v2Parser = {}

---@param filename string
---@return id3.Tags
function M.Mid3v2Parser.read(filename)
  local command = { 'mid3v2', filename }
  local lines = job.run(command)

  local values = {}
  for line in lines do
    -- mid3v2 displays tags in the format TAG=value.
    local frame, value = string.match(line, '^(%w+)%=(.*)$')
    if frame and value then
      -- id3v2.4 spec uses TDRC for release date.
      -- mid3v2 will write the TYER frame as TDRC aswell to keep tags up to date correctly, so
      -- handling them the same saves having to explictly handle how they are displayed separately
      -- from the other of the providers.
      if frame == 'TDRC' then
        frame = 'TYER'
      end

      values[frame] = value
    end
  end

  return common.make_tags(frame_to_name, values)
end

---@param filename string
---@param tags id3.Tags
---@return nil
function M.Mid3v2Parser.write(filename, tags)
  local command = { 'mid3v2' }

  -- Build command-line arguments of the form:
  -- --FRAME_1 VALUE_1 ... --FRAME_N VALUE_N
  for _, tag in ipairs(tags) do
    local frame = name_to_frame[tag.name]
    if frame then
      util.table_insert(command, string.format('--%s', frame), tag.value)
    end
  end

  table.insert(command, filename)
  job.run(command)
end

return M
