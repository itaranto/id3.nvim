local M = {}

---@param command string[]
---@return fun(): string?
function M.run(command)
  local comp = vim.system(command, { text = true }):wait()
  assert(
    comp.code == 0,
    string.format(
      'wait job for command "%s"\n%s\ncode: %d',
      table.concat(command, ' '),
      comp.stderr,
      comp.code
    )
  )

  return comp.stdout:gmatch('[^\n]+')
end

return M
