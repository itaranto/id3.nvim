local M = {}

---@param id_to_name { [string]: string }
---@param values { [string]: string }
---@return id3.Tags
function M.make_tags(id_to_name, values)
  local tags = {}

  for id, name in pairs(id_to_name) do
    local value = values[id]
    if not value then
      value = ''
    end

    table.insert(tags, { name = name, value = value })
  end

  return tags
end

return M
