local common = require('id3.common')
local job = require('id3.job')
local util = require('id3.util')

local M = {}

---@type { [string]: string }
local id_to_name = {
  TITLE       = 'Title',
  ARTIST      = 'Artist',
  ALBUMARTIST = 'Album artist',
  ALBUM       = 'Album',
  DATE        = 'Year',
  TRACKNUMBER = 'Track',
  TRACKTOTAL  = 'Track total',
  GENRE       = 'Genre',
}

---@type { [string]: string }
local name_to_id = {
  Title            = 'TITLE',
  Artist           = 'ARTIST',
  ['Album artist'] = 'ALBUMARTIST',
  Album            = 'ALBUM',
  Year             = 'DATE',
  Track            = 'TRACKNUMBER',
  ['Track total']  = 'TRACKTOTAL',
  Genre            = 'GENRE',
}

---@class flac.MetaFLACParser
M.MetaFLACParser = {}

---@param filename string
---@return id3.Tags
function M.MetaFLACParser.read(filename)
  local command = { 'metaflac', '--export-tags-to', '-', filename }
  local lines = job.run(command)

  local values = {}
  for line in lines do
    local id, value = line:match('^(.+)=(.+)$')
    if id and value then
      values[id] = value
    end
  end

  return common.make_tags(id_to_name, values)
end

---@param filename string
---@param tags id3.Tags
---@return nil
function M.MetaFLACParser.write(filename, tags)
  local command = { 'metaflac' }

  -- Build command-line arguments of the form:
  -- --remove-tag ID_1 --set-tag ID_1=VALUE_1 ... --remove-tag ID_N --set-tag ID_N=VALUE_N
  for _, tag in ipairs(tags) do
    local id = name_to_id[tag.name]
    if id then
      util.table_insert(
        command,
        '--remove-tag', id,
        '--set-tag', string.format('%s=%s', id, tag.value)
      )
    end
  end

  table.insert(command, filename)
  job.run(command)
end

return M
