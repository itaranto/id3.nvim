local M = {}

---@param filename string
---@param tags id3.Tags
---@return string[]
local function parse_tags(filename, tags)
  -- The filename must go first.
  local header = {
    filename,
    string.rep('=', #filename),
    '',
  }

  local body = {}
  for _, tag in ipairs(tags) do
    table.insert(body, string.format('%s: %s', tag.name, tag.value))
  end

  local lines = {}
  for _, line in ipairs(header) do
    table.insert(lines, line)
  end

  for _, line in ipairs(body) do
    table.insert(lines, line)
  end

  return lines
end

---@param parser id3.Reader
---@param filename string
---@return nil
function M.read_tags(parser, filename)
  local tags = parser.read(filename)
  local lines = parse_tags(filename, tags)
  vim.api.nvim_buf_set_lines(0, 0, -1, true, lines)
end

---@param lines string[]
---@return id3.Tags
local function parse_lines(lines)
  local tags = {}
  for _, line in ipairs(lines) do
    local name, value = line:match('^(.+): (.+)$')
    if name and value then
      table.insert(tags, { name = name, value = value })
    end
  end

  return tags
end

---@param src string
---@param dst string
---@return nil
local function rename_file(src, dst)
  local status, err = os.rename(src, dst)
  assert(status ~= nil, string.format('rename "%s" to "%s"\n%s', src, dst, err))
end

---@param parser id3.Parser
---@param filename string
---@return nil
function M.write_tags(parser, filename)
  local lines = vim.api.nvim_buf_get_lines(0, 0, -1, true)
  local tags = parse_lines(lines)
  parser.write(filename, tags)

  -- Handle file rename and reload the buffer.
  local new_filename = lines[1]
  if filename ~= new_filename then
    rename_file(filename, new_filename)
    M.read_tags(parser, new_filename)
    vim.api.nvim_buf_set_name(0, new_filename)
  end

  vim.opt.modified = false
end

return M
